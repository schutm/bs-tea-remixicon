const fs = require("fs");
const fsWalk = require('@nodelib/fs.walk');
const path = require('path')
const util = require("util");

const { toCamelCase, toSnakeCase } = require("js-convert-case");
const xmlParser = require("xml-js");

const svgo = new (require("svgo/lib/svgo"))({
  plugins: [
    { removeUselessStrokeAndFill: false },
    { removeXMLNS: true },
    { removeAttrs: { attrs: [ "data-.*", "aria.*" ] } },
    { convertStyleToAttrs: true },
  ],
});

function ocamlize(functionName, xmlJs) {
  function dekeyword(word) {
    const keywords = [
      "and",
      "as",
      "assert",
      "asr",
      "begin",
      "class",
      "constraint",
      "do",
      "done",
      "downto",
      "else",
      "end",
      "exception",
      "external",
      "false",
      "for",
      "fun",
      "function",
      "functor",
      "if",
      "in",
      "include",
      "inherit",
      "initializer",
      "land",
      "lazy",
      "let",
      "lor",
      "lsl",
      "lsr",
      "lxor",
      "match",
      "method",
      "mod",
      "module",
      "mutable",
      "new",
      "nonrec",
      "object",
      "of",
      "open",
      "or",
      "private",
      "rec",
      "sig",
      "struct",
      "then",
      "to",
      "true",
      "try",
      "type",
      "val",
      "virtual",
      "when",
      "while",
      "with",
    ];

    return keywords.includes(word) ? word + "'" : word;
  }

  function prefixDigit(word) {
    const firstChar = (word && word.length > 0) ? word.charAt(0) : "";
    const prefix = (firstChar >= "0" && firstChar <= "9") ? "_" : "";

    return prefix + word;
  }

  function validIdentifier(identifier) {
    return prefixDigit(dekeyword(toSnakeCase(functionName)));
  }

  function mapAttributes(attributes) {
    return Object.keys(attributes || {}).map(
      (key) => `A.${dekeyword(toCamelCase(key))} "${attributes[key]}"`
    );
  }

  function mapElement(element) {
    const name = element.name;
    const attributes = mapAttributes(element.attributes);
    const elements = (element.elements || []).map((element) =>
      mapElement(element)
    );

    return `E.${name} [ ${attributes.join("; ")} ] [ ${elements.join("; ")} ]`;
  }

  const svgElement = xmlJs.elements[0];
  const svgAttributes = mapAttributes(svgElement.attributes);
  const vdom = svgElement['elements'].map(mapElement);

  return `let ${validIdentifier(functionName)} properties =
  let module E = Tea.Svg in
  let module A = Tea.Svg.Attributes in
  E.svg ([ ${svgAttributes.join("; ")} ] @ properties) [ ${vdom.join("; ")} ]`;
}

(async () => {
  const iconsPath = path.resolve(path.dirname(require.resolve("remixicon/package.json")), "icons");


  const icons = await Promise.all(
    fsWalk.walkSync(iconsPath).filter((entry) => entry.dirent.isFile()).map((entry) =>
      (async () => {
        const data = fs.readFileSync(entry.path, "utf8");
        return await svgo.optimize(data).then(({ data, info }) => {
          const xmlJs = xmlParser.xml2js(data);
          const svg = xmlJs.elements[0];
          svg.attributes["transform"] = "translate(1,0)";
          svg.attributes["fill"] = "currentColor";

          const ocaml = ocamlize(path.basename(entry.name, '.svg'), xmlJs);

          return {
            data,
            xmlJs,
            ocaml,
          };
        });
      })()
    )
  );

  fs.mkdir("./src", {}, () => {
    fs.writeFile(
      "./src/remixIcon.ml",
      icons.map((icon) => icon.ocaml).join("\n\n"),
      (err) => {
        if (err) throw err;
      }
    );
  });
})();
