Release 0.0.5
-------------
Bugfix (forgot closing bracket).

Release 0.0.4
-------------
Translate the icons with oen pixel to the right, to better center the icons.

Release 0.0.3
-------------
Add fillcolor to apply the expected to the icon.

Release 0.0.2
-------------
Prefix digits with a valid identifier (_).

Release 0.0.1
-------------
Initial release to automatically import the Remix Icon (System) and create bucklescript bindings.
